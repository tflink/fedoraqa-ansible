Fedora QA Ansible Playbooks
###########################

`Ansible <http://www.ansibleworks.com/>`_ is a tool for managing servers that
is quite a bit less complicated than some other alternatives (puppet, chef etc.)
but can be just as powerful.

This repository contains the playbooks that we use to manage the qa hosts which
aren't directly managed by Fedora infra.


Getting Set Up
==============

To get set up, you need to have a few things:

 * A linux box - ansible might run on a windows box but I suspect you're setting
   yourself up for a difficult road.

 * A specific directory structure - there are statically defined paths used here
   and unless you want to redefine all those variables, it's much easier to just
   follow the directory convention

     - ``/srv/ansible`` is the root directory for everything

     - ``/srv/ansible/qa`` is a checkout of this repository

      - ``/srv/ansible/private/qa`` is where all the secrets live (passwords,
        non-public variables etc.)

      - ``/srv/ansible/private/qa/$ENVNAME.yml`` is where all of the private
        variables like passwords, ports etc. are stored. A sanitized template is
        provided so you don't have to guess at what all those passwords are

      - ``/srv/ansible/private/qa/certs/$ENVNAME/`` is where all of the ssh keys
        and other certs (ssl, gpg etc.) live. I generally create one set of keys
        and certs per environment.

  * Ansible - either install from a distro package or any other way.

Running Playbooks
=================

The playbooks here are designed to be pretty flexible - you should be able to use
them with multiple environments with no changes other than variables. There are
a couple of variables which may change, depending on how you have everything set up.

 * ``$INVENTORY`` this is an inventory file which describes the hosts you will
   be working with.

 * ``$PATH_TO_SSH_KEY`` is the path to the ssh private key that you will be
    using to connect to the host(s)

 * ``$ENVTYPE`` the environment type you'll be using - this will select the proper
    variables for the environment you're working in in you differentiate between
    production, staging, devel etc.

 * ``$SSH_PORT`` is the ssh port to connect over if you're not using the
    standard ssh port (22/tcp)

 * ``$TARGET`` can be either a host name or a group name for hosts. For a setup
    this simple, it is most often a hostname

 * ``$PLAYBOOK`` the yml playbook that you want to run

 * ``$USER`` the user that you want to connect as on the remote host - default
    is your current local user.

 * ``$VIRTHOST`` is the virthost for a vm, if you're using a vm

AutoQA Client Commands
----------------------

``ansible-playbook -i $INVENTORY --private-key $PATH_TO_SSH_KEY -e "envtype=$ENVTYPE ansible_ssh_port=$SSH_PORT virthost=$VIRTHOST target=$TARGET" playbooks/$PLAYBOOK -u $USER``

Blockerbugs Dev Command
-----------------------

``ansible-playbook -i $INVENTORY --private-key $PATH_TO_SSH_KEY -e "envtype=$ENVTYPE ansible_ssh_port=$SSH_PORT target=$TARGET" playbooks/$PLAYBOOK -u $USER``

Utility Actions
===============

There are a couple of utility actions that you may want to use

Generating passwords
--------------------

The passwords stored in variable files are pre-encryped and salted, to encrypt
the password ``terriblepassword`` using the salt ``SomeSalt`` use the following
command:


``python -c 'import crypt; print crypt.crypt("terriblepassword", "$1$SomeSalt$")'``

