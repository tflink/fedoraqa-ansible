QA Host information
###################


This is a record of host data for the qa hosts.

qa01.qa
=======
  * 16G memory

  * 2x Intel Xeon E5540 (4 cores each)

  * 2x 146G SAS Disks (RAID1 from HW)


qa02.qa
=======
  * 16G memory

  * 2x Intel Xeon E5540 (4 cores each)

  * 2x 146G SAS Disks (RAID1 from HW)


qa03.qa
=======
  * 18G memory

  * 2x Intel Xeon E5540 (4 cores each)

  * 2x 146G SAS Disks (RAID1 from HW)


qa04.qa
=======
  * 18G memory

  * 2x Intel Xeon E5540 (4 cores each)

  * 2x 146G SAS Disks (RAID1 from HW)


qa05.qa
=======
  * 18G memory

  * 2x Intel Xeon E5540 (4 cores each)

  * 2x 146G SAS Disks (RAID1 from HW)


qa06.qa
=======
  * 18G memory

  * 2x Intel Xeon E5540 (4 cores each)

  * 2x 146G SAS Disks (RAID1 from HW)

