#!/usr/bin/bash

# run the initial config - it is assumed that ssh access for root is already set up
#echo 'ansible-playbook -i $HOSTENV playbooks/pysprings.yml -e "target=$HOSTNAME envtype=$HOSTENV" -u root -t base'
echo $HOSTENV
echo $HOSTENV
echo ansible-playbook -i $HOSTENV playbooks/pysprings.yml -e "target=$HOSTNAME envtype=$HOSTENV" -u root -t base

# now that the initial user/ssh setup is done, do the rest of it
echo ansible-playbook -i $HOSTENV playbooks/pysprings.yml -e "target=$HOSTNAME envtype=$HOSTENV ansible_ssh_port=$SSH_PORT" -u $USERNAME
